from jsonschema import validate, ValidationError, SchemaError

schema = {
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "http://example.com/mqqt.schema.json",
    "title": "MQQT",
    "description": "An MQQT message",
    "type": "object",
    "properties": {
        "api_version": {
            "type": "string",
            "minLength": 1,
            "maxLength": 5,
             },
        "device_battery": {
            "type": "integer",
            "minimum": 0,
            "maximum": 100
        },
        "device_model": {
            "enum": [
                "Mark One",
                "Mark Two",
                "Mark Three"
            ]
        },
        "event_type":{
            "enum": [
                "scan",
                "info"
            ]
        },
        "scan_duration": {
            "type": "number",
            "minimum": 0,
            "multipleOf": 0.0001
        },

    },
    "required": ["api_version","device_battery","device_model","event_type","scan_duration"]
}


inputJson = \
    {
    "api_version": "0.5",
    "device_battery": 94,
    "device_model": "Mark Two",
    "event_type": "scan",
    "scan_duration": 0.99,
    }


try:
    validate(inputJson, schema)
    print('\nValidation OK!')

except SchemaError as e:
    print('There is an error with the schema')

except ValidationError as e:
    print("------ Error : ------\n")
    print(e)

    # print("------ Error in parameter : ------\n")
    # print(e.absolute_path)

    # print("------ Full path to parameter : -----\n")
    # print(e.absolute_schema_path)